package hk.ust.cse.comp107x.physvid;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class notepad_activity extends AppCompatActivity {
    private ListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notepad);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mListView = findViewById(R.id.listview_notepad);
    }

    public void ShowNewNote(View v){
        Intent newNoteActivity = new Intent(this,NoteActivity.class);
        startActivity(newNoteActivity);
    }


    @Override
    protected void onResume(){
        super.onResume();
        mListView.setAdapter(null);
        ArrayList<Note> notes = Utilities.getAllSavedNotes(this);

        if(notes == null || notes.size()==0){
            Toast.makeText(this,"You have no saved notes!",Toast.LENGTH_SHORT).show();
            return;
        }
        else{
            NoteAdapter noteAdapter = new NoteAdapter(this,R.layout.item_note,notes);
            mListView.setAdapter(noteAdapter);
        }
    }
}
