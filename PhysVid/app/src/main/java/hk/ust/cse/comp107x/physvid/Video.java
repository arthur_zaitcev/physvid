package hk.ust.cse.comp107x.physvid;

/**
 * Created by jeffa on 07-Jan-18.
 */

public class Video {
    private String description;
    private String name;
    private String path;


    public Video(String description, String name, String path) {
        this.description = description;
        this.name = name;
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
