package hk.ust.cse.comp107x.physvid;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final int REQUEST_TAKE_GALLERY_VIDEO = 1;
    Dialog myDialog;
    private String filemanagerstring;
    private String selectedImagePath;
    private Context context;
    private String pickedVideoUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        context = this.getApplicationContext();
        myDialog = new Dialog(this);
    }

    public void ShowPopup(View v){
        TextView txtClose;
        myDialog.setContentView(R.layout.video_add_popup);

        final EditText videoName = myDialog.findViewById(R.id.input_video_name);
        final EditText videoDescription = myDialog.findViewById(R.id.input_video_description);

        txtClose = (TextView)myDialog.findViewById(R.id.textclose);
        Button addVideo= (Button)myDialog.findViewById(R.id.add_video_button);
        addVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Video video = new Video(videoDescription.getText().toString(),videoName.getText().toString(),pickedVideoUrl);

            }
        });
        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        ImageView imageUpload = (ImageView)myDialog.findViewById(R.id.image_add);

//        File imgFile = new File("/storage/3638-3238/DCIM/Camera/20171219_103200.mp4");
//        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//        imageUpload.setImageBitmap(myBitmap);

        imageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videoPickIntent = new Intent(Intent.ACTION_PICK);
                videoPickIntent.setType("video/*");
                startActivityForResult(Intent.createChooser(videoPickIntent, "Please pick a video"),REQUEST_TAKE_GALLERY_VIDEO);
            }
        });
        myDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != REQUEST_TAKE_GALLERY_VIDEO)
            return;
        if (resultCode != Activity.RESULT_OK)
        {

        }
        //video url is here save it in a
        pickedVideoUrl = getRealPathFromUri(context, data.getData());
        //
        System.out.println("URL : "+pickedVideoUrl);
    }
    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        }
        if(id == R.id.nav_notepad){
            Intent notepadAct = new Intent(this, notepad_activity.class);
            startActivity(notepadAct);
        }
        if(id == R.id.nav_notif){
            Intent notif = new Intent(this, NotificationsActivity.class);
            startActivity(notif);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
