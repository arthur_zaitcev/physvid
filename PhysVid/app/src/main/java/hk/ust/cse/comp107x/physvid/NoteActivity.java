package hk.ust.cse.comp107x.physvid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NoteActivity extends AppCompatActivity {

    private EditText mTitle;
    private EditText mContent;
    private Button saveBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        mTitle = findViewById(R.id.note_title);
        mContent = findViewById(R.id.note_content);
        saveBtn = findViewById(R.id.save_note);

    }

    public void saveNote(View view){
        Note note = new Note(System.currentTimeMillis(),mTitle.getText().toString(),mContent.getText().toString());
        try{
            boolean b  =Utilities.saveNote(this,note);
            Toast.makeText(this,"Saved  "+  b,Toast.LENGTH_SHORT).show();
            finish();
        }
        catch (Exception e){
            Toast.makeText(this,"error while saving "+e.getMessage(),Toast.LENGTH_SHORT).show();

        }
    }
}
