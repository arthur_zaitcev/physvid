package hk.ust.cse.comp107x.physvid;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import gr.net.maroulis.library.EasySplashScreen;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EasySplashScreen config = new EasySplashScreen(SplashScreen.this)
                .withFullScreen()
                .withTargetActivity(MainActivity.class)
                .withSplashTimeOut(5000)
                .withBackgroundColor(Color.parseColor("#2f333f"))
                .withLogo(R.mipmap.logo_white_m)
                .withHeaderText("")
                .withFooterText("Copyright 2017")
                .withBeforeLogoText("")
                .withAfterLogoText("PhysVid");

    //Set text color

        config.getHeaderTextView().setTextColor(Color.WHITE);
        config.getFooterTextView().setTextColor(Color.WHITE);
        config.getAfterLogoTextView().setTextColor(Color.WHITE);
        config.getBeforeLogoTextView().setTextColor(Color.WHITE);

        //logo
        config.getLogo().setMaxHeight(500);
        config.getLogo().setMaxWidth(500);

        //text after logo
        config.getAfterLogoTextView().setTextSize(46);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/Raleway-Regular.ttf");
        config.getAfterLogoTextView().setTypeface(face);

        View view = config.create();

        setContentView(view);

    }

}
